require('jquery.scrollto');

  module.exports = function() {

    let see = function (event, setup) {

      let guid = setup.data.options.guid;

      let fadeInSpeed  = setup.data.options.in||333;
      let fadeOutSpeed = setup.data.options.out||333;
      let scrollSpeed  = setup.data.options.scroll||333;

      let target = $('.' + guid).get(0);

      let fadeInFinished = () => { $(document).trigger( setup.meta.reply, {meta:{}, data:{}} ); }

      let fadeOutFinished = () => { $(target).animate({ opacity: 1 }, fadeInSpeed, "linear", fadeInFinished , 200); }

      let blink = () => {

        $(target).animate({ opacity: 0 }, fadeOutSpeed, "linear", fadeOutFinished );

      }; // go

      $(window).stop(true).scrollTo(target, { duration: scrollSpeed, onAfter: blink, });

    };

    $( document ).off( "see" );
    $( document ).on( "see", see );

  };
