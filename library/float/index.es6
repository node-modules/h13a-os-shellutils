require("jquery-ui/draggable");

let highestOrder = function () {
  let baseZindex = 100;
  $('.ui-draggable').each(function () {
    let myZindex = parseInt($(this).css('z-index'));
    if (myZindex >= baseZindex) {
      baseZindex = myZindex + 1;
    }
  });
  return baseZindex;
};

module.exports = function () {

  let float = function (event, setup) {

    let min = setup.data.options.min || 0;
    let guid = setup.data.settings[0] || setup.data.options.guid;
    let target = $('.' + guid);

    // Is the screen big enough for this operation?
    if($(document).width() < min) {

      // the screen is too small for normal operation.

      let scroll = setup.data.options.scroll || false;
      // should the command just scroll to the tango location?
      if(scroll) {

        $(window).stop(true).scrollTo(target.get(0), { duration: 500, onAfter: ()=>{
          // reply
          $(document).trigger(setup.meta.reply, { meta: {}, data: {} });
        }});
        return;
      }else{
        // just reply
        return $(document).trigger(setup.meta.reply, { meta: {}, data: {} });
       }
    }



    let bringToFront = function () {
      if (target.hasClass('ui-draggable')) target.css('z-index', highestOrder());
    }

    let dragStartup = function (event) {


      target.css({
        'width': target.width(),
        'height': target.height()
      });

      target.draggable($.extend(setup.data.options, {
        handle: '.drag-handle',
        cursor: "crosshair",
        stack: ".fui-card",
        opacity: 0.85,
      }));

      $('.drag-close', target).on("click", dragShutdown);

      target.on("click", bringToFront);
      target.css('position', 'fixed');

      let floating = $('.ui-draggable').length;
      let top = setup.data.options.top || ((floating) * 32) + 'px'
      let left = setup.data.options.left || ((floating - 1) * 128) + 'px';

      target.css('left', left);
      target.css('top', top);
      target.css('z-index', highestOrder());
      target.parent().css({padding:0, margin:0}); // toggle parent padding
    }

    let dragShutdown = function (event) {
      if (target.hasClass('ui-draggable')) target.draggable("destroy");
      target.css('top', '');
      target.css('left', '');
      target.css('width', '');
      target.css('height', '');
      target.css('z-index', '');
      target.css('position', '');
      target.off("click", bringToFront);
      $('.drag-close', target).off("click", dragShutdown);
      target.parent().css({padding:"", margin:""}); // toggle parent padding
    }


    if (target.hasClass('ui-draggable')) {
      dragShutdown();
    } else {
      dragStartup();
    }

    $(document).trigger(setup.meta.reply, { meta: {}, data: {} }); // respond with noop

  };

  $(document).off("float");
  $(document).on("float", float);

};
