require('jquery.scrollto');
module.exports = function() {
  let fire = function (event, setup) {
    let className = setup.data.settings[0] || setup.data.options.guid;
    let name = setup.data.settings[1] || setup.data.options.name;
    let target = $('.' + className).trigger(name.replace(/\-/g,''));
    $(document).trigger(setup.meta.reply, { meta: {}, data: {} });
   };
  $( document ).off( "fire" );
  $( document ).on( "fire", fire );
};
