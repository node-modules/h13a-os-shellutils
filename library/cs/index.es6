const util = require("h13a-util");

module.exports = function(options) {


  let cs = function(event, setup) {

    let cardStyleUpper = setup.data.options.max || 3;
    let cardStyleClass = setup.data.options.cls || 'fui-card';

    let $componentNode = $('.' + setup.data.options.guid);

    let widget = util.widgetFromNode(options.namespace, $componentNode);

    if (widget) {

      let existingStyle = util.getNumericClassValue({
        prefix: cardStyleClass + "-style",
        element: $componentNode
      });

      let nextStyle = existingStyle + 1;

      if (nextStyle > cardStyleUpper) {
        nextStyle = 0;
      }

      util.setNumericClassValue({
        prefix: cardStyleClass + "-style",
        element: $componentNode,
        number: nextStyle
      });

      $(document).trigger(setup.meta.reply, {
        data: {
          style: nextStyle
        }
      });
    }

  }

  $( document ).off( "cs" );
  $( document ).on( "cs", cs );

};
