module.exports = function() {

  let sleep = function(event, setup) {

    // sleep 5
    let seconds = ((setup.data.settings[0]||0) * 1000);

    setTimeout(()=>{

      $(document).trigger( setup.meta.reply, {meta:{}, data:{}} );

    }, seconds);

  };

  $( document ).off( "sleep" );
  $( document ).on( "sleep", sleep );

};
