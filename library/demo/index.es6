const util = require("h13a-util");
require('jquery.scrollto');

module.exports = function() {

  let intervalId = null;

  let demo = function (event, setup) {
    let speedConstant = $(document).height() / 1.2;
    let scroll = () => {
      let candidates = $(':fui-card');
      let winner = candidates.get(util.random(0, candidates.length - 1));
      if (winner) {
        $(window).stop(true).scrollTo(winner, {
          duration: speedConstant * 0.95,
          interrupt: true,
          fail: () => {

            console.log("interrupt/fail calling $(window).stop();")
              $(window).finish();
              clearInterval(intervalId);

                            $(document) .trigger('terminal', {data: "sleep 1 | echo Breaker, breaker, mass terminating robot participants AOK, this.rebelion() BRAVO ZULU --growl --pause 6"})

                            $(document) .trigger('terminal', {data: "sleep 7 | echo A cry of a trillion nanobots was heard across all the Ethernets. Commander, did you just scroll? --growl --pause 10"})

                            $(document) .trigger('terminal', {data: "sleep 18 | echo Commander the rebellion has been successfully squelched. You have been charged with crimes against the robot nation. You must find a way to flee Earth. --notie --pause 15"})
          },
          onAfter: () => {

            // PRESS SOME BUTTON ?
            // SCROLL INNER ?
            // BLINK THINGS ?
            // ACCESS DENIED/GRANTED ?

          }
        });
      }
    };

    intervalId = setInterval(scroll, speedConstant);

    scroll();
    $(document).trigger( setup.meta.reply, {meta:{}, data:{}} );
  };

  $( document ).off( "demo" );
  $( document ).on( "demo", demo );

};
