const util = require("h13a-util");

module.exports = function() {

  let rot = function(event, setup) {
    let str = setup.data.settings.join(" ");

    // Usage Note Example
    let data = str ? str.replace(/[a-z]/gi, function(s) {
      return String.fromCharCode(s.charCodeAt(0) + (s.toLowerCase() < 'n' ? 13 : -13));
    }) : "Usage: `rot13 URYYB` prints HELLO, `rot13 HELLO` prints URYYB see: https://en.wikipedia.org/wiki/ROT13 for more";
    $(document).trigger( setup.meta.reply, {meta:{}, data} );
  };

  $( document ).off( "rot" );
  $( document ).on( "rot", rot );

  // Alias Example
  $( document ).off( "rot13" );
  $( document ).on( "rot13", rot );

};
