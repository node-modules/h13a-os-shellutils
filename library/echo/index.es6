require('sweetalert');

const toastr = require('toastr/toastr.js');
const notie = require('notie');

module.exports = function() {

  let echo = function(event, setup) {

    let msg = "";

    if (setup.data.settings && setup.data.settings.length == 1) {
      // there was just a single string
      msg = setup.data.settings[0];
    } else if (setup.data.settings && setup.data.settings.length) {
      // multiple argumants
      msg = setup.data.settings.join(" ");
    } else {
      // malformaed data
    }

    // print only if there is something to say;
    if (msg) {

      let text = JSON.stringify(msg).replace(/</g, '&lt;').replace(/>/g, '&gt;');
      let icon = setup.data.options.icon || 'microphone';
      let pause =  parseInt(setup.data.options.pause)*1000 || (3*1000);

      $('.console-position.terminal-console').prepend(`<div class="text-${setup.data.options.type||'info'}"><i class="fa fa-${icon}"></i> ${text}</div>`);

      if (setup.data.options.growl) {

        toastr.info( text , 'Info', {timeOut: pause});
      }

      if (setup.data.options.alert && swal) {
        swal({
          title: "",
          text,
          timer: pause,
          showConfirmButton: false
        });
      }

      if (setup.data.options.notie && notie) {
        notie.alert(4, text, pause/1000);
      }

    }

    $(document).trigger( setup.meta.reply, {meta:{}, data:{}} );

  };

  $( document ).off( "echo" );
  $( document ).on( "echo", echo );

};
