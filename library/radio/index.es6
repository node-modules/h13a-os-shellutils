module.exports = function() {

  let radio = function(event, setup) {

    let eventName = [setup.data.options.channel, (setup.data.options.action||'poke')].join("");

    let recordSeparatorString = setup.data.options.rs||"&";
    let valueSeparatorString  = setup.data.options.vs||"=";
    let spaceSeparatorString  = setup.data.options.ss||"+";

    recordSeparatorString = recordSeparatorString.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    valueSeparatorString  = valueSeparatorString.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    spaceSeparatorString  = spaceSeparatorString.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");

    let recordSeparator = new RegExp(recordSeparatorString, 'g');
    let valueSeparator  = new RegExp(valueSeparatorString);
    let spaceSeparator  = new RegExp(spaceSeparatorString, "g");

    let outgoing = {
      data:[]
    };

    let records = setup.data.options.data.split(recordSeparator);

    records.forEach((record)=>{
      let [path, value] = record.split(valueSeparator);

      if(value){
        value = value.replace(spaceSeparator, ' ');
      }

      if(setup.data.options.prefix){
        path = setup.data.options.prefix + '.' + path;
      }

      outgoing.data.push({path,value});
    });

    $(document).trigger(eventName, outgoing);
    $(document).trigger( setup.meta.reply, {meta:{}, data:{}} );

  };

  $( document ).off( "radio" );
  $( document ).on( "radio", radio );

};
