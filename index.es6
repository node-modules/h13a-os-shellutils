
const commands = require.context("./library/", true, /[a-z0-9-]+\/index.es6$/);

module.exports = function(options){

$(function(){

  commands.keys().map(command => {
    try {
      let installer = commands(command);

      installer(options);

    }catch(e){
      console.log('Error while loading %s command', command, e);
    }
  });

  // console.dir( $._data( $(document).get(0), 'events' ) );

});

}
